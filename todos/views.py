from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions

from todos.serializers import TodoSerializer
from todos.models import Todo
# Create your views here.

class TodoViewSet(ModelViewSet):
    permission_classes = [permissions.AllowAny]
    serializer_class=TodoSerializer
    queryset = Todo.objects.all()
