from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from todos.models import Todo

class TodoSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Todo
        fields = [
            'pk',
            'title',
            'description',
            'image',
        ]