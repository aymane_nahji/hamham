from django.db import models

# Create your models here.
class Todo(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    image = models.ImageField(blank=True,null=True)