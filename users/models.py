from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class User(AbstractUser):
    sex = models.CharField(max_length=10,choices=(
        ('male','male'),
        ('female','female')
    ),null=True)
