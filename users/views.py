from django.shortcuts import render,redirect
from django.contrib.auth import logout
from django.urls import reverse_lazy
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from users.models import User
from users.serializers import UserSerializer

# Create your views here.
def logout_view(request):
    try:
        logout(request)
    except:
        pass
    return redirect(reverse_lazy('index'))

class UserViewSet(ModelViewSet):
    serializer_class=UserSerializer
    queryset = User.objects.all()
    http_method_names=['get']

    def list(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)