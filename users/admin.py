from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from users import models

# Register your modelslike this.
admin.site.register(models.User)
# class StudentResource(resources.ModelResource):
#     class Meta:
#         model=Student
# class StudentAdmin(ImportExportModelAdmin):
#     resource_class=StudentResource
#     list_display=[field.name for field in Student._meta.fields]


#admin.site.register(Student,StudentAdmin)