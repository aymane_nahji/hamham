from rest_framework import serializers
from users.models import User
from rest_flex_fields import FlexFieldsModelSerializer

class UserSerializer(FlexFieldsModelSerializer):
    # commands = serializers.PrimaryKeyRelatedField(many=True,read_only=True,source="command_set")
    class Meta:
        model=User
        fields=[
            'pk',
            'username',
            'first_name',
            'last_name',
            'email',
            'sex',
        ]
        # expandable_fields = {
        #     'commands':('products.serializers.CommandSerializer',{'many':True,'source':'command_set'})
        # }
