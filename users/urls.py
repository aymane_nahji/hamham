from django.urls import include, path
from decorator_include import decorator_include
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpRequest
from users import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('me',views.UserViewSet,'me')


def is_superuser(user):
    return user.is_superuser


urlpatterns = [
    path('', include('dj_rest_auth.urls')),
    path('singup/',decorator_include([user_passes_test(is_superuser)],'dj_rest_auth.registration.urls')),
    path('user_logout/', views.logout_view, name='user_logout')
] + router.urls
